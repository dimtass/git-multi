Pull/Push/Sync a single repo to multiple providers
----

This script is for my personal usage, but you can modify it and
re-use it for your own purposes, if you like. This script has a
specific usage, though. I keep my golden repos in bitbucket and
then I'm using `github` and `gitlab` to clone my repos. The reason
for that is security and also more people can have access to them.

Since I'm having several repos now, I need to keep them in sync
and also allow other people to create PRs and push code to any
of those repos. That creates a problem on how you sync these
repos after something new is merged on some of the repos.

> Note: This scripts assumes that all repos share the exact
same project name.

You can use the script to pull a change from the origin repo
and then push to others. To do this:

```sh
git pull --recursive git@github.com:dimtass/project-name.git
cd project-name
cp /path/to/git-multi .
./git-multi
git fetch --all
# Do your changes but do not git add git-multi
# And after you add and commit, then run:
git push
```

The git push now will push the changes to all the repos.
